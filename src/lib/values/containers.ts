import type { Containers, Container, AllocateStore } from "$lib"
import { ResourceId, ResourceType, ContainerId } from "$lib";
import { writable } from "svelte/store";

const createContainerStore = (container : Container) :  AllocateStore<Container> =>  {
    const { subscribe, set, update } = writable<Container>(container);
    const add = (amount : number) => {
        update((r) => ({...r, owned : r.owned + amount}));
    }
    const reallocateStorage = (method : ResourceId, amount : number) => {
        update((r) => {
            const newAllocation = (r.storeAllocation[method] || 0) + amount;
            return {...r, storeAllocation : {...r.storeAllocation, [method] : newAllocation}};
        });
    }
    return {
        subscribe,
        set,
        update,
        add,
        reallocateStorage,
    }
}

const baseContainers = 
[
    {
        id: ContainerId.bucket,
        name: 'Ore Bucket',
        description: 'A bucket that holds ore.',
        cost : {[ResourceId.copperBar]: 5},
        containFlags : ResourceType.bar | ResourceType.ore | ResourceType.solid,
        stores :{ [ResourceId.copper] : 15 },
        storeAllocation : {},
        owned : 0,
    } as Container,
    {
        id: ContainerId.battery,
        name: 'Small Battery',
        description: 'A small battery that holds energy.',
        cost : {[ResourceId.copperBar]: 25},
        containFlags : ResourceType.fuel,
        stores :{ [ResourceId.electricty] : 15 },
        storeAllocation : {},
        owned : 0,
    } as Container,
]


export const containers : Containers = baseContainers.reduce((acc, container) => {
    return {
        ...acc,
        [container.id] : createContainerStore(container),
    }
}, {}) as Containers;
