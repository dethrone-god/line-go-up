import { writable } from 'svelte/store';
import type { Resources, Resource, ResourceStore } from '$lib';
import { ResourceType, ResourceId } from '$lib';

const createResourceStore = (resource : Resource) :  ResourceStore<Resource> =>  {
    const { subscribe, set, update } = writable(resource);
    const add = (value : number) => {
        update((r) => {
            return {...r, value: r.value + value}
        });
    }
    return {
        add,
        subscribe,
        set,
        update,
    }
}

const baseResources = [
    {
        id: ResourceId.copper,
        name: 'Copper',
        description: 'A soft, malleable metal.',
        value: 0,
        resourceFlags : ResourceType.solid & ResourceType.ore,
        overloadDecay : 1,
    } as Resource,
    {
        id : ResourceId.copperBar,
        name : 'Copper Bar',
        description : 'A bar of copper.',
        value : 0,
        resourceFlags : ResourceType.solid & ResourceType.bar,
        overloadDecay : 1,
    } as Resource,
    {
        id : ResourceId.electricty,
        name : 'Electricty',
        description : 'Electricy in your network.',
        value : 0,
        resourceFlags : ResourceType.fuel,
        overloadDecay : 1,
    } as Resource,
    {
        id : ResourceId.coal,
        name : 'Coal',
        description : 'Coal',
        value : 4,
        resourceFlags : ResourceType.solid & ResourceType.fuel,
        overloadDecay : 0,
    } as Resource,
]

export const resources : Resources = baseResources.reduce((acc, resource) => {
    return {
        ...acc,
        [resource.id] : createResourceStore(resource),
    }
}, {}) as Resources;
