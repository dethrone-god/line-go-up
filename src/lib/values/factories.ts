import { ResourceId, FactoryId, ResourceType } from "$lib";
import type { ProductionStore, Factory, Factories } from "$lib";
import { writable } from "svelte/store";


const createFactoryStore = (factory : Factory) :  ProductionStore<Factory> =>  {
    const { subscribe, set, update } = writable(factory);
    const add = (value : number) => {
        update((r) => ({...r, owned : r.owned + value}));
    }
    const reallocateStorage = (method : ResourceId, amount : number) => {
        update((r) => {
            const newAllocation = (r.storeAllocation[method] || 0) + amount;
            return {...r, storeAllocation : {...r.storeAllocation, [method] : newAllocation}};
        });
    }
    const reallocateProduction = (method : ResourceId, amount : number) => {
        update((r) => {
            const newAllocation = (r.productionAllocation[method] || 0) + amount;
            return {...r, productionAllocation : {...r.productionAllocation, [method] : newAllocation}};
        });
    }
    return {
        subscribe,
        set,
        add,
        update,
        reallocateStorage,
        reallocateProduction,
    }
}

const baseFactories = [
{
    id: FactoryId.mine,
    name: 'Mine',
    description: 'A mine that produces ore slowly.',
    owned: 2,
    cost : {[ResourceId.copperBar] :5},
    inputFlags : ResourceType.fuel,
    outputFlags : ResourceType.bar,
    production: 
        {
            [ResourceId.copper]: {
                inputs : {[ResourceId.electricty] : 1},
                outputs : {[ResourceId.copper] : 1}
            },
            [ResourceId.coal]: {
                inputs : {[ResourceId.electricty] : 1},
                outputs : {[ResourceId.coal] : 1}
            },
        },
    productionAllocation : {
        [ResourceId.copper] : 1,
        [ResourceId.coal] : 1,
    },
    storeAllocation : {
        [ResourceId.copper] : 1,
        [ResourceId.coal] : 1,
    },
    stores : {
        [ResourceId.copper] : 5,
        [ResourceId.coal] : 5,
    },
} as Factory,
{
    id: FactoryId.smelter,
    name: 'Ore Smelter',
    description: 'A smelter that turns ore into bars.',
    owned: 1,
    cost : {[ResourceId.copperBar] :5},
    inputFlags : ResourceType.fuel | ResourceType.ore,
    outputFlags : ResourceType.bar,
    production: 
    {
        [ResourceId.electricty] :
            {inputs : {
                [ResourceId.copper] : 2,
                [ResourceId.electricty] : 1,
            },
            outputs : {[ResourceId.copperBar] : 2},
        },
        [ResourceId.coal] :
            {inputs : {
                [ResourceId.copper] : 1,
                [ResourceId.coal] : 1,
            },
            outputs : {[ResourceId.copperBar] : 1},
        },
    },
    productionAllocation : {
    },
    storeAllocation : {
        [ResourceId.copperBar] : 1,
    },
    stores : {
        [ResourceId.copper] : 5,
        [ResourceId.copperBar] : 5,
    },
} as Factory,
{
    id: FactoryId.powerPlant,
    name: 'Power Plant',
    description: 'A plant that turns fuel into electricty.',
    owned: 1,
    cost : {[ResourceId.copperBar] : 50},
    inputFlags : ResourceType.fuel | ResourceType.ore,
    outputFlags : ResourceType.bar,
    production: 
    {
        [ResourceId.coal] :
            {inputs : {
                [ResourceId.coal] : 2,
            },
            outputs : {
                [ResourceId.electricty] : 5,
            },
        },
    },
    productionAllocation : {
        [ResourceId.coal] : 1,
    },
    storeAllocation : {
        [ResourceId.electricty] : 1,
    },
    stores : {
        [ResourceId.electricty] : 25,
    },
} as Factory,
]
export const factories : Factories = baseFactories.reduce((acc, resource) => {
    return {
        ...acc,
        [resource.id] : createFactoryStore(resource),
    }
}, {}) as Factories;
