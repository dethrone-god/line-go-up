// place files you want to import through the `$lib` alias in this folder.
import type { Writable } from "svelte/store";
import { get } from "svelte/store";

export enum ResourceId {
    copper,
    coal,
    copperBar,
    electricty,
}

export enum FactoryId {
    mine,
    smelter,
    factory,
    powerPlant
}

export enum ContainerId {
    bucket,
    tank,
    battery,
}

export const ResourceIds = Object.values(ResourceId).filter(e=> typeof e !== 'string') as unknown as ResourceId[];
export const FactoryIds = Object.values(FactoryId).filter(e=> typeof e !== 'string') as unknown as FactoryId[];
export const ContainerIds = Object.values(ContainerId).filter(e=> typeof e !== 'string') as unknown as ContainerId[];

export type ResourceValueDict = {
    [key in ResourceId]? : number;
}
export type ResourceValue = {
    name : string;
    resourceId : ResourceId;
    value : number;
}

export const ResourcesToArray = (res? : ResourceValueDict) : ResourceValue[] => {
    if(!res) return [];
    return Object.entries(res).map(([resourceId, value]) => 
    ({name : ResourceId[Number(resourceId)], resourceId : Number(resourceId), value}))
    ;
};
export const ProductionMethodsToArray = (res? : ProductionMethods) : (ProductionMethod & {name:string, resourceId : ResourceId})[] => {
    if(!res) return [];
    return Object.entries(res).map(([rid, value]) => {
        const resourceId = Number(rid);
        return {resourceId, name : ResourceId[resourceId], inputs : ResourcesToArray(value.inputs), outputs : ResourcesToArray(value.outputs)}
    });
};

export const getProductionMethod = (res? : ProductionMethodDict) : ProductionMethod => {
    if(!res) return {inputs : [], outputs : []};
    return {outputs : ResourcesToArray(res.outputs), inputs : ResourcesToArray(res.inputs)} 
};

export const getFactoryResourceProduction = ( factory : Factory, resourceId : ResourceId) : number => {
    const net =  getFactoryNetProduction(factory);
    return net?.[resourceId] ?? 0;
}

export const getTotalResourceCapacity = (buyable : Buyable, resourceId : ResourceId) : number => {
    const allocation = buyable?.storeAllocation[resourceId];
    if(!allocation || allocation === 0) return 0;
    const storeCapacity = buyable?.stores[resourceId];
    if(!storeCapacity || storeCapacity === 0) return 0;
    return allocation * storeCapacity;
}

export const getFactoryNetProduction = (factory : Factory) : ResourceValueDict => {
    const productionAllocation = ResourcesToArray(factory?.productionAllocation ?? {});
    return productionAllocation.reduce((acc, allocation) => {
        const {resourceId} = allocation;
        const method = factory.production[resourceId];
        const multiplier = allocation.value;
        const { inputs, outputs } = getProductionMethod(method);
        inputs?.forEach((input) => {
            acc[input.resourceId] = (acc[input.resourceId] ?? 0) - input.value * multiplier;
        });
        outputs?.forEach((output) => {
            acc[output.resourceId] = (acc[output.resourceId] ?? 0) + output.value * multiplier;
        });
        return acc;
    }, {} as ResourceValueDict);
}

export const runFactory = (factory : Factory, resources : Resources, containers : Containers) => {
    const productionAllocation = ResourcesToArray(factory?.productionAllocation ?? {});
    productionAllocation.forEach((allocation) => {
        const {resourceId} = allocation;
        const method = factory.production[resourceId];
        const multiplier = allocation.value;
        const { inputs, outputs } = getProductionMethod(method);
        if(!inputs || inputs?.every(e=> (e.value * multiplier) <= get(resources[e.resourceId]).value)){
            const canProduce = outputs?.find(e=> get(resources[e.resourceId]).value + (e.value * multiplier) <= getTotalResourceCapacity(factory, e.resourceId));
            if(canProduce){
                outputs?.forEach((output) => {
                    resources[output.resourceId].add(output.value * multiplier);
                });
                inputs?.forEach((input) => {
                    resources[input.resourceId].add(-input.value * multiplier);
                });
            }
        }
    });
}

export type ProductionMethods = {
    [key in ResourceId]? : ProductionMethodDict;
}

export type ProductionMethod = {
    inputs?: ResourceValue[];
    outputs?: ResourceValue[];
}

export type ProductionMethodDict = {
    inputs?: ResourceValueDict;
    outputs?: ResourceValueDict;
}

export enum ResourceType {
    solid = 1 << 0,
    liquid = 1 << 1,
    ore = 1 << 2,
    bar = 1 << 3,
    fuel = 1 << 4,
}

export interface Resource {
    id: ResourceId;
    name: string;
    description: string;
    resourceFlags: ResourceType;
    value: number;
    overloadDecay: number;
}


export interface Buyable {
    name: string;
    cost : ResourceValueDict;
    storeAllocation : ResourceValueDict;
    owned : number;
    stores : ResourceValueDict
}

export interface Factory extends Buyable {
    id: FactoryId;
    description: string;
    inputFlags: ResourceType;
    outputFlags : ResourceType;
    owned: number;
    productionAllocation : ResourceValueDict;
    production : ProductionMethods;
}

export interface Container extends Buyable {
    id: ContainerId;
    description: string;
    owned: number;
    containFlags : ResourceType;
}

export interface ResourceStore<T> extends Writable<T> {
    add : (value : number) => void;
}

export interface AllocateStore<T extends Buyable> extends Writable<T> {
    add: (value : number) => void;
    reallocateStorage: (method : ResourceId, amount : number) => void;
}
export interface ProductionStore<T extends Factory> extends AllocateStore<T> {
    reallocateProduction: (method : ResourceId, amount : number) => void;
}

export type Containers = {
    [key in ContainerId]: AllocateStore<Container>;
}
export type Factories = {
    [key in FactoryId]: ProductionStore<Factory>;
}

export type Resources = {
    [key in ResourceId]: ResourceStore<Resource>;
}