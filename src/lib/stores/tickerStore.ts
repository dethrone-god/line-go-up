import { resources, factories, containers } from '$lib/values';
import { FactoryIds, runFactory } from '$lib';
import { get } from 'svelte/store';

export const resourceTick = () => {
    //console.time("factory-tick");
    FactoryIds.forEach((factoryId) => {
        runFactory(get(factories[factoryId]), resources, containers);
    });
    //console.timeEnd("factory-tick");
}