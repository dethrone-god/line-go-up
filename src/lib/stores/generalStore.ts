import { factories, containers } from '$lib/values';
import type { ResourceId, Factory } from '$lib';
import { getProductionMethod, getFactoryResourceProduction, getTotalResourceCapacity } from '$lib';
import { derived } from 'svelte/store';
import type {Readable } from 'svelte/store';

interface Stats {
    production : number;
    consumption : number;
    totalCapacity : number;
}

const containerFactories = [...Object.values(factories), ...Object.values(containers)];

export const getResourceStats = (resourceId : ResourceId) : Readable<Stats> => {
    return derived(containerFactories, ([...arr]) => {
        return arr.reduce((acc, store) => {
            const resCapacity = getTotalResourceCapacity(store, resourceId);
            const factory = store as Factory;
            let production = 0;
            let consumption = 0;
            if(factory.production){
                const net = getFactoryResourceProduction(factory, resourceId);
                if(net > 0){
                    production = net;
                }else{
                    consumption = net;
                }
            }
            return {
                production : acc.production + production,
                consumption : acc.consumption + consumption,
                totalCapacity : acc.totalCapacity + resCapacity,
            } as Stats;
        }, {production : 0, consumption : 0, totalCapacity : 0} as Stats);
    });
};

/*
const resourceStores = Object.values(resources);

export const getResource = (resourceId : ResourceId) : Readable<Resource> => {
    return derived(resourceStores, ([...arr]) => {
        return arr.find((r) => r.id === resourceId) as Resource;
    });
};
*/